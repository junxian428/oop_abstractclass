abstract class Animal {
    private double weight;

    public abstract String sound();

    public static void main(String[] args){
        Animal a1 = new Tiger();
        Animal a2 = new Chicken();

        System.out.println(a1.sound());
        System.out.println(a2.sound());
        System.out.println(((Chicken) a2).howToEat());

        Fruit buah1 = new Orange();
        Fruit buah2 = new Apple();

        System.out.println(buah1.howToEat());
        System.out.println(buah2.howToEat());

    }
}

class Tiger extends Animal{
    public String sound(){
        return "rawrrr";
    }
}

interface Edible{
    public abstract String howToEat();
}

class Chicken extends Animal implements Edible{
    @Override
    public String sound(){
        return "rawrrr";
    }

    @Override
    public String howToEat() {
        // TODO Auto-generated method stub
        return "Eat with rice - become nasi ayam";
    }
}

abstract class Fruit implements Edible{
   
}

class Orange extends Fruit {
    @Override
    public String howToEat() {
        // TODO Auto-generated method stub
        return "peel and eat";
    }
}

class Apple extends Fruit {
    @Override
    public String howToEat() {
        // TODO Auto-generated method stub
        return "Cut and eat";
    }
}
